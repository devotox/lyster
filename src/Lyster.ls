###############################################################################################################
##
## Issues with Solutions like Masonry
## It uses absolute positioning and this means elements are removed from natural document flow
## Can cause problems when other elements are near position elements
##
## This solution aims to figure out the minimal movement an element has to make to move up the grid stack
## Future: Infinite Scrolling, Faster Algorithm, Use React, Extend algorithm to rearrange grid to best possible component position
##
#################################################################################################################
##
## Ambiguous Variables
## w: width h: height 
## 
## A config object can be passed into the `init` function 
## The default value in `@defaults` will be used anywhere a config key is missing
## Look at the `@defaults` to see available configuration options that can be passed in the config object
##
##
## To allow for more minute height selections to pass to the randomizer 
## The height_diff config value uses linear growth rather than a growth multiplier
## Setting the height_diff to the same value as the min_component_height can achieve the multiplier effect
##
##
## A growth multiplier has to be used for the width in the pool of choices available to always align perfectly
## have to be a multiple of the lowest common width plus the margin in between the images
##
##
## Everything in this file is set in an anonymous function and thus would not pollute the global scope
## unless `global` is set to true in the config object in which case window.Lyster is set to the Lyster object
## 
##
## Setting `debug` to true in the config object can display useful information to the console
##
## 
## JQuery Selector function is only ever run once and object cached for later use
##
##
## Minimal Row Gap Conundrum ( MRGC )
## Not being able to find a CSS solution to the row gap issue ( now dubbed MRGC )
## I opted to use a JS solution which can be turned off by setting `fix_mrgc` to false in the config object
## ( I know i sicken me too )
##
##
## Clicking on the header Reloads the application ( Reload can also take in a new config object )
##
##
## All height and width values are random but selected from a pool of choices
##
##
###############################################################################################################

'use strict'

class Lyster

	defaults: 
		columns: 4 # Number of columns to populate
		margin: 50 # Margin in between each image # Must match margin in @styl
		debug: false # Sends debugging information to the console
		global: true # Sets `this` to window.Lyster
		fix_mrgc: true # Fixes Minimal Row Gap Conundrum *Note might cause peformance issues in large sets
		responsive: false # Sets whether to have a responsive container or always force space for column width
		
		max_components: 100 # Maximum number of components ( images ) to be retrieved
		height_diff: 50 # Height differentials for height choices to be made available to randomizer
		width_multiplier: 2 # Multiplier factor for width choices to be made available to randomizer
		number_of_widths: 2 # Sets amount of width choices to be made abailable to the randomizer
		number_of_heights: 10 # Sets amount of height choices to be made abailable to the randomizer
		min_component_width: 200 # Minumum image width in pixels
		min_component_height: 200 # Minimum image height in pixels

		body: \body # selector for app placement
		grid_id: \grid # Sets image grid id
		header_id: \header # Sets header container id 
		container_id: \container # Sets main container id
		image_api: "http://lorempixel.com/{w}/{h}" # Image API Replaces: {w} - width {h}- height
		
		component_element: \img # Set component element type
		component_class: \component # Set Component class
		filler_class: \filler # Set filler class

		arrow: '\u21e7' # Sets Arrow using a html entity
		scroll_text: "Scroll To Top" # Sets scroll text
		dimensions: null # You can set static dimensions here
		scroll_button_color: \white # Scroll button color to make sure it can be viewed on any background
		scroll_button_animation_time: 1000 # Time to scroll to top in milliseconds
		container_margin_top: 200 # Margin top of container in pixels
		
		header_hide: false # Toggles header visibility
		header1: "Lyster \u21ba" # Sets header H1
		header2: "Using Jade - Stylus - Livescript - Gulp" # Sets header H2

	config: {}

	components: {}

	dimensions: {}

	(config) ~>
		@destroy!
		@set_config config
		@create_container!
		@set_dimensions!
		@set_global!
		@set_style!
		@actions!
		@create!
		@mrgc!

	create: ->
		@debug 'Lyster Components', @components
		for i from 1 to @config.max_components
			w = @random @dimensions.width.length
			@set_component_dimensions @append_component do
				@dimensions.width[  w  ] + ( w * @config.margin )
				@dimensions.height[ @random @dimensions.height.length ]

	mrgc: ->
		return unless @config.fix_mrgc

		console.time 'MRGC' if @config.debug
		for own row, components of @components.grid_rows
			row = parseInt row; continue unless row > 0 
			prev_row = @components.grid_rows[ row - 1 ]

			for own column, component of components
				column = parseInt column; continue unless row > 0
				@move_component component, @calculate_move component, prev_row
		console.timeEnd 'MRGC' if @config.debug

	calculate_move: (component, prev_row) ->
		
		lowest_prev_comp = null

		for own prev_col, prev_comp of prev_row

			if ( 
				( component.dimensions.left is prev_comp.dimensions.left or 
					component.dimensions.right is prev_comp.dimensions.right ) and 
				( component.dimensions.columns <= prev_comp.dimensions.columns ) 
			) then lowest_prev_comp = prev_comp; break


			if ( ( component.dimensions.right > prev_comp.dimensions.left ) and ( prev_comp.dimensions.right > component.dimensions.left ) ) or 
				( ( component.dimensions.left < prev_comp.dimensions.right ) and ( prev_comp.dimensions.left < component.dimensions.right ) )
				lowest_prev_comp = prev_comp if not lowest_prev_comp or prev_comp.dimensions.bottom > lowest_prev_comp?.dimensions.bottom

		return 0 unless lowest_prev_comp
		return component.dimensions.top - ( lowest_prev_comp.dimensions.bottom + @config.margin )

	move_component: (component, move_by) ->
		return unless move_by
		move_by += @config.margin if ( component.dimensions.top - move_by) is ( @config.container_margin_top + @config.margin )

		component.dimensions.top = component.dimensions.top - move_by
		component.dimensions.bottom = component.dimensions.top + component.dimensions.height
		component.element?.css? top: -( move_by )

	set_component_dimensions: (elem, re_calc) ->
		elem.css \top, 0 if re_calc
		
		dimensions = elem.offset! 
		dimensions.height = elem.height!; dimensions.width = elem.width!
		dimensions.columns = Math.floor dimensions.width / @config.min_component_width

		dimensions.right = dimensions.left + dimensions.width
		dimensions.bottom = dimensions.top + dimensions.height

		@components.grid_components ?= []; @components.grid_rows ?= []
		@components.grid_components.push component = element: elem, dimensions: dimensions
		@set_row component, dimensions

	set_row: (component, dimensions) ->
		prev_row = @components.grid_rows[ @components.grid_rows.length - 1 ]
		if prev_row?[0]?.dimensions?.top is dimensions?.top then prev_row.push component
		else @normalize_prev_row prev_row; @components.grid_rows.push [ component ]

	normalize_prev_row: (prev_row) ->
		return unless prev_row

		new_dimensions = 
			columns: 1
			height: - @config.margin
			top: prev_row[0].dimensions.top
			bottom: prev_row[0].dimensions.top
			width: @config.min_component_width

		col_total = 0
		for column, component of prev_row 
			col_total += component.dimensions.columns
			
		return if col_total is @config.columns

		for i from 1 to ( @config.columns - col_total )
			prev_row.push element: null, dimensions: $.extend true, {
				left: prev_row[ prev_row.length - 1 ].dimensions.right + @config.margin
				right: prev_row[ prev_row.length - 1 ].dimensions.right +  @config.margin + @config.min_component_width
			}, new_dimensions

	set_dimensions: ->
		@dimensions = that if @config.dimensions

		for key in <[ width height ]> 
			@dimensions[key] = @set_dimension key, @config["min_component_#{key}"]

	set_dimension: (dimension, value) ->
		arr = Array( @config["number_of_#{dimension}s"] + 1)
			.join( @config["min_component_#{dimension}"] + "|" )
			.split("|").map (c, i) ~>
				return unless c; switch dimension
					when \width then parseInt(c) * ( i + 1 )
					when \height then parseInt(c) + ( ( i + 1 ) * @config.height_diff ) + @config.height_diff
		arr.pop!; @debug "Component #{dimension} selection pool", arr; return arr

	set_global: -> window?.Lyst = @ if @config.global

	set_config: (config) ->
		unless config
			@config = @defaults 
			return @debug 'Lyster Config', @config

		for own key, value of @defaults
			@config[key] = config?[key] ?= value

		@debug 'Lyster Config', @config

	set_style: ->
		return @container!.css width: \90% if @config.responsive
		_width = @dimensions.width[0] * @config.columns
		_margin = @config.margin * @config.columns
		@container!.css width: _width + _margin

	header: ->
		@components.header or= $ '<div />', id: @config.header_id
			.append $('<h1 />').text @config.header1
			.append $('<h2 />').text @config.header2
			.[if @config.header_hide then \hide else \show ]!

	random: (max=2) -> Math.floor Math.random! * max

	debug: (txt, obj) -> console.log txt, ': ', obj if @config.debug

	src: (w, h) -> @config.image_api.replace "{w}", w .replace "{h}", h

	window: -> @components.window or= $ window

	body: -> @components.body or= $ @config.body

	grid: -> @components.grid or= $ '<div />', id: @config.grid_id

	container: -> 
		@components.container or= $ '<div />', id: @config.container_id
			.css margin-top: "#{@config.container_margin_top}px"	

	create_container: ->
		@body!.append( @header! ).append( @container!.append @grid! )

	append_component: (w, h) -> @grid!.append c = @create_component w, h; return c

	create_component: (w=200, h=300) ->
		$ "<#{@config.component_element} />", class: @config.component_class, src: @src w, h .css do
			position: \relative, width: "#{w}px", height: "#{h}px", margin: "0 #{@config.margin}px #{@config.margin}px 0",

	append_scroll_button: ->
		@components.scroll_button or= $ '<div />', id: \scroll_button, text: @config.arrow .css color: @config.scroll_button_color
			.append @components.scoll_text or = $ '<p />' id: \scroll_text, text: @config.scroll_text

		return if @container!.has @components.scroll_button .length
		@container!.append @components.scroll_button
		@configure_scroll_button!

	destroy_scroll_button: -> @components.scroll_button?.remove?!

	configure_scroll_button: ->
		@components.to_scroll or= $ 'html, body'
		@components.scroll_button.bind 'click.Lyster', (e) ~>
			@components.to_scroll.animate scrollTop: \0 , @config.scroll_button_animation_time
			e.stopImmediatePropagation!	

	actions: ->
		@window!.bind 'scroll.Lyster', ~>
			if @window!.scrollTop! < @viewport!.height
				@destroy_scroll_button!
			else @append_scroll_button!

		resize_timeout = null
		@header!.find 'h1, h2' .bind 'click.Lyster', ~> @reload!
		if @config.responsive then @window!.bind 'resize.Lyster', ~>
			clearTimeout resize_timeout; resize_timeout = setTimeout (~> @recalculate! ), 500

	viewport: (refresh) ->
		return that if not refresh and @dimensions.viewport

		a = \inner; win = window
		if ( not \innerWidth in win ) 
			a = \client; win = document.documentElement or document.body
		@dimensions.viewport = width: win["#{a}Width"], height: win["#{a}Height"]

	destroy: -> 
		@body!?.empty?!
		window?.Lyst = null
		@window!?.unbind? \.Lyster
		for key in <[ components dimensions ]> then @[key] = {}

	reload: (config) ->
		@destroy?!
		@window!.scrollTop 0
		new Lyster config or= @config

	recalculate: ->		
		grid_components = $.extend true, [], @components.grid_components
		@components.grid_rows = []; @components.grid_components = []

		for own i, component of grid_components 
			@set_component_dimensions component.element, true
		return @mrgc!

## Set to module exports or to window object
module.exports = Lyster if module?.exports
@Lyster = Lyster if @window
