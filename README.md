Lyster

### Commands -
	`gulp - Runs all
	gulp dev - Runs all and watches
	gulp watch - Watches all folders
	gulp test - Runs all lints / tests (if there were some)
	gulp prod - Runs all and cleans dev
	gulp templates - Compiles HTML ['html', 'jade', 'dust']
	gulp images - Compiles Images - ['jpg', 'png', 'gif', 'bmp']
	gulp styles - Compiles Styles - ['css', 'less', 'sass', 'stylus']
	gulp scripts - Compiles Scripts - ['javascript', 'livescript', 'coffeescript', 'typescript']
	gulp publish - Updates package.json & bower.json creates release, creates git tag, and publishes to npm & bower`


### Caveats -
	`Files / Folders are retrieved in alphabetical order`



###############################################################################################################
##
## Issues with Solutions like Masonry
## It uses absolute positioning and this means elements are removed from natural document flow
## Can cause problems when other elements are near position elements
##
## This solution aims to figure out the minimal movement an element has to make to move up the grid stack
## Future: Infinite Scrolling, Faster Algorithm, Use React, Extend algorithm to rearrange grid to best possible component position
##
#################################################################################################################
##
## Ambiguous Variables
## w: width h: height 
## 
## A config object can be passed into the `init` function 
## The default value in `@defaults` will be used anywhere a config key is missing
## Look at the `@defaults` to see available configuration options that can be passed in the config object
##
##
## To allow for more minute height selections to pass to the randomizer 
## The height_diff config value uses linear growth rather than a growth multiplier
## Setting the height_diff to the same value as the min_component_height can achieve the multiplier effect
##
##
## A growth multiplier has to be used for the width in the pool of choices available to always align perfectly
## have to be a multiple of the lowest common width plus the margin in between the images
##
##
## Everything in this file is set in an anonymous function and thus would not pollute the global scope
## unless `global` is set to true in the config object in which case window.Lyster is set to the Lyster object
## 
##
## Setting `debug` to true in the config object can display useful information to the console
##
## 
## JQuery Selector function is only ever run once and object cached for later use
##
##
## Minimal Row Gap Conundrum ( MRGC )
## Not being able to find a CSS solution to the row gap issue ( now dubbed MRGC )
## I opted to use a JS solution which can be turned off by setting `fix_mrgc` to false in the config object
## ( I know i sicken me too )
##
##
## Clicking on the header Reloads the application ( Reload can also take in a new config object )
##
##
## All height and width values are random but selected from a pool of choices
##
##
###############################################################################################################