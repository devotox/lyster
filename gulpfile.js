
// ==================================== REQUIRES ==================================================== //

var gulp = require('gulp');
var pkg = require('./package.json');
var $ = require('gulp-load-plugins')({camelize: true});

var handleError = function(err) {
  console.error(err.toString());
  this.emit('end');
}

// ==================================== CONFIG ==================================================== //

var template_config = {
  pretty: true,
  locals: {
    pkg: pkg
  }
};

var vendor = {
  node: [
    'node_modules/jquery/**/*',
  ],
  bower: [
  ]
};

var paths = {
  vendor: vendor,
  manifest: ['build/**/*'],
  images: ['res/images/**/*'],
  styles: ['res/styles/**/*'],
  html: ['res/html/**/*.html'],
  scripts: ['res/libs/**/*', 'res/scripts/**/*'],
  templates: ['res/templates/**/*', 'res/html/**/*'],
  dust: ['res/templates/**/*.dust', 'res/html/**/*.dust'],
  jade: ['res/templates/**/*.jade', 'res/html/**/*.jade'],

  css: ['res/styles/**/*.css'],
  less: ['res/styles/**/*.less'],
  sass: ['res/styles/**/*.scss'],
  stylus: ['res/styles/**/*.styl'],

  javascript: ['res/libs/**/*.js', 'res/scripts/**/*.js'],
  livescript: ['res/libs/**/*.ls', 'res/scripts/**/*.ls'],
  typescript: ['res/libs/**/*.ts', 'res/scripts/**/*.ts'],
  coffeescript: ['res/libs/**/*.coffee', 'res/scripts/**/*.coffee'],
  react: ['res/components/**/*.jsx', 'res/libs/**/*.jsx', 'res/scripts/**/*.jsx'],

  app: {
    react: ['src/**/*.jsx'],
    javascript: ['src/*/*.js'],
    livescript: ['src/**/*.ls'],
    typescript: ['src/**/*.ts'],  
    coffeescript: ['src/**/*.coffee']
  },

  test: {
    action: 'run',
    all: ['test/*.js'],
    karmarc: ".karmarc",
    mocharc: ".mocharc",
    mocha: ['test/*.mocha.js'],
    karma: ['test/*.karma.js'],
    jasmine: ['test/*.jasmine.js']
  },

  publish: {
    root: './',
    bumpType: 'patch',
    packages: ['./package.json', './bower.json']    
  },  

  jshint: {
    jshintrc: '.jshintrc'
  },

  build: {
    maps: '../maps',
    prod_folder: ['build'],
    dev_folder: ['dev-build'],
    folders: {
      app: 'build/app',
      vendor: 'build/vendor',
      images: 'build/images',
      styles: 'build/styles',
      scripts: 'build/scripts',
      templates: 'build/templates'
    },
    filenames: {
      manifest: [ pkg.name, 'manifest' ].join('.'),
      styles: [ pkg.name, 'styles.css' ].join('.'),
      scripts: [ pkg.name, 'scripts.js' ].join('.'),
      app: [ pkg.name, 'scripts.js' ].join('.'),
      min: {
        styles: [ pkg.name, 'styles.css' ].join('.'),
        scripts: [ pkg.name, 'scripts.js' ].join('.'),
        app: [ pkg.name, 'scripts.js' ].join('.')
      }
    },
    dev: {
      folders: {
        app: 'dev-build/app',
        vendor: 'dev-build/vendor',
        images: 'dev-build/images',
        styles: 'dev-build/styles',
        scripts: 'dev-build/scripts',
        templates: 'dev-build/templates'
      },
      glob: {
        app: ['dev-build/app/**/*'],
        styles: ['dev-build/images/**/*'],
        styles: ['dev-build/styles/**/*.css'],
        scripts: ['dev-build/scripts/**/*.js']
      }
    }
  }
};

// ==================================== SCRIPTS ================================================== //
// Script Tasks
gulp.task('javascript', function(){
  return gulp.src(paths.javascript)
    .pipe($.sourcemaps.init())
    .pipe($.sourcemaps.write(paths.build.maps))
    .pipe(gulp.dest(paths.build.dev.folders.scripts))
});

gulp.task('coffeescript', function(){
  return gulp.src(paths.coffeescript)
    .pipe($.sourcemaps.init())
    .pipe($.coffee())
    .pipe($.sourcemaps.write(paths.build.maps))
    .pipe(gulp.dest(paths.build.dev.folders.scripts))
});

gulp.task('livescript', function(){
  return gulp.src(paths.livescript)
    .pipe($.sourcemaps.init())
    .pipe($.livescript())
    .pipe($.sourcemaps.write(paths.build.maps))
    .pipe(gulp.dest(paths.build.dev.folders.scripts))
});

gulp.task('scripts-minify', ['javascript', 'coffeescript', 'livescript'], function(cb) {
  return gulp.src(paths.build.dev.glob.scripts)
    .pipe($.concat(paths.build.filenames.scripts))
    .pipe(gulp.dest(paths.build.dev.folders.scripts))
    .pipe($.rename(paths.build.filenames.min.scripts))
    .pipe($.uglify())
    //.pipe($.stripDebug())
    .pipe(gulp.dest(paths.build.folders.scripts))
    .pipe($.size({title: 'Scripts:'}))
});

gulp.task('scripts', function(cb){
  $.runSequence('clean', 'scripts-minify', cb);
});

// ==================================== STYLES ================================================== //
// Style Tasks
gulp.task('css', function(){
  return gulp.src(paths.css)
    .pipe($.sourcemaps.init())
    .pipe($.sourcemaps.write(paths.build.maps))
    .pipe(gulp.dest(paths.build.dev.folders.styles))
});

gulp.task('less', function(){
  return gulp.src(paths.less)
    .pipe($.less())
    .pipe(gulp.dest(paths.build.dev.folders.styles))
});

gulp.task('sass', function(){
  return gulp.src(paths.sass)
    .pipe($.sourcemaps.init())
    .pipe($.sass())
    .pipe($.sourcemaps.write(paths.build.maps))
    .pipe(gulp.dest(paths.build.dev.folders.styles))
});

gulp.task('stylus', function(){
  return gulp.src(paths.stylus)
    .pipe($.sourcemaps.init())
    .pipe($.stylus())
    .pipe($.sourcemaps.write(paths.build.maps))
    .pipe(gulp.dest(paths.build.dev.folders.styles))
});

gulp.task('styles-minify', ['css', 'less', 'stylus'], function(cb) {
  return gulp.src(paths.build.dev.glob.styles)
    .pipe($.concat(paths.build.filenames.styles))
    .pipe($.autoprefixer())
    .pipe(gulp.dest(paths.build.dev.folders.styles))
    .pipe($.rename(paths.build.filenames.min.styles))
    .pipe($.minifyCss())
    .pipe($.bless())
    .pipe(gulp.dest(paths.build.folders.styles))
    .pipe($.size({title: 'Styles: '}))
});

gulp.task('styles', function(cb){
  $.runSequence('clean', 'styles-minify', cb);
});

// ==================================== HTML ================================================== //
// HTML Tasks
gulp.task('html', function(){
  return gulp.src(paths.html)
    .pipe(gulp.dest(paths.build.dev_folder[0]))
    .pipe($.minifyHtml())
    .pipe(gulp.dest(paths.build.prod_folder[0]))
    .pipe($.size({title: 'HTML:   '}))
});

gulp.task('jade', function() {
  return gulp.src(paths.jade)
    .pipe($.jade(template_config))
    .pipe(gulp.dest(paths.build.dev_folder[0]))
    .pipe($.minifyHtml())
    .pipe(gulp.dest(paths.build.prod_folder[0]))
    .pipe($.size({title: 'JADE:   '}))
});

gulp.task('dust', function() {
  return gulp.src(paths.dust)
    .pipe($.dust(template_config))
    .pipe(gulp.dest(paths.build.folders.templates))
    .pipe(gulp.dest(paths.build.folders.templates))
    .pipe($.size({title: 'DUST:   '}))
});

gulp.task('templates', function(){
  $.runSequence('clean', ['html', 'jade', 'dust'])
});

// ==================================== MANIFEST ================================================== //
// Make App Cache Manifest
gulp.task('manifest', function(){
  return gulp.src(paths.manifest)
    .pipe($.manifest({
      hash: true,
      preferOnline: true,
      network: ['http://*', 'https://*', '*'],
      filename: paths.build.filenames.manifest,
      exclude: [ paths.build.filenames.manifest ]
     }))
    .pipe(gulp.dest(paths.build.prod_folder[0]))
    .pipe($.size({title: 'Manifest:'}))
});

// ==================================== IMAGES ================================================== //
// Copy all static images
gulp.task('images', ['clean'], function() {
  return gulp.src(paths.images)
    // Pass in options to the task
    .pipe($.cache($.imagemin({
      optimizationLevel: 5,
      progressive: true,
      interlaced: true,
    })))
    .pipe(gulp.dest(paths.build.folders.images))
    .pipe(gulp.dest(paths.build.dev.folders.images))
    .pipe($.size({title: 'Images:'}))
});

// ==================================== CLEAN ==================================================== //
// Clean Tasks
gulp.task('clean-dev', function() {
  return gulp.src(paths.build.dev_folder, { read: false })
    .pipe($.rimraf())
});

gulp.task('clean-prod', function() {
  return gulp.src(paths.build.prod_folder, { read: false })
    .pipe($.rimraf())
});

// ==================================== TEST ==================================================== //
// Lint & Test
gulp.task('lint', function () {
  return gulp.src(paths.build.dev.glob.scripts)
    .pipe($.jshint(paths.jshint.jshintrc))
    .pipe($.jshint.reporter('jshint-stylish'));
});

gulp.task('mocha', function () {
  return gulp.src(paths.test.mocha)
    .pipe($.mocha({ 
      reporter: 'spec',      
      globals: {
          should: require('should'),
          expect: require('expect')
      }
    })
    .on("error", handleError));
});

gulp.task('karma', function() {
  return gulp.src(paths.test.karma)
    .pipe($.karma({
      configFile: paths.test.karmarc,
      action: paths.test.action
    }))
    .on('error', handleError)
});

gulp.task('jasmine', function () {
    return gulp.src(paths.test.jasmine)
      .pipe($.jasmine({
        reporter: 'default',
      }))
      .on('error', handleError);
});

// ==================================== WATCH ================================================== //
// Rerun the task when a file changes
gulp.task('watch', function() {
  gulp.watch(paths.scripts, ['test']);
  gulp.watch(paths.images, ['default']);
  gulp.watch(paths.styles, ['default']);
  gulp.watch(paths.templates, ['default']);
});

// ==================================== VENDOR ================================================== //
// Add Vendor Files From node_modules / bower_components that need more than just a require

gulp.task('vendor_bower', function(){
  return gulp.src(paths.vendor.bower, { base: './bower_components' })
    .pipe(gulp.dest(paths.build.dev.folders.vendor))
    .pipe(gulp.dest(paths.build.folders.vendor))
    .pipe($.size({title: 'Vendor: '}));
});

gulp.task('vendor_node', function(){
  return gulp.src(paths.vendor.node, { base: './node_modules' })
    .pipe(gulp.dest(paths.build.dev.folders.vendor))
    .pipe(gulp.dest(paths.build.folders.vendor))
    .pipe($.size({title: 'Vendor: '}));
});

gulp.task('vendor', function(){
  $.runSequence('clean', ['vendor_node', 'vendor_bower']);
});

// ==================================== APP ================================================== //
// Build App
gulp.task('app-javascript', function(){
  return gulp.src(paths.app.javascript)
    .pipe($.sourcemaps.init())
    .pipe($.sourcemaps.write(paths.build.maps))
    .pipe(gulp.dest(paths.build.dev.folders.app))
});

gulp.task('app-coffeescript', function(){
  return gulp.src(paths.app.coffeescript)
    .pipe($.sourcemaps.init())
    .pipe($.coffee())
    .pipe($.sourcemaps.write(paths.build.maps))
    .pipe(gulp.dest(paths.build.dev.folders.app))
});

gulp.task('app-livescript', function(){
  return gulp.src(paths.app.livescript)
    .pipe($.sourcemaps.init())
    .pipe($.livescript())
    .pipe($.sourcemaps.write(paths.build.maps))
    .pipe(gulp.dest(paths.build.dev.folders.app))
});

gulp.task('app-minify', ['app-javascript', 'app-coffeescript', 'app-livescript'], function(cb) {
  return gulp.src(paths.build.dev.glob.app)
    .pipe($.concat(paths.build.filenames.app))
    .pipe(gulp.dest(paths.build.dev.folders.app))
    .pipe($.rename(paths.build.filenames.min.app))
    .pipe($.uglify())
    //.pipe($.stripDebug())
    .pipe(gulp.dest(paths.build.folders.app))
    .pipe($.size({title: 'APP:'}))
});

gulp.task('scripts', function(cb){
  $.runSequence('clean', 'scripts-minify', cb);
});
gulp.task('app', function(cb){
  $.runSequence('clean', 'app-minify', cb);  
})

// ==================================== PUBLISH ================================================== //
// Publish App

gulp.task('bump', function () {
  return gulp.src(paths.publish.packages)
    .pipe($.bump({type: paths.publish.bumpType}))
    .pipe(gulp.dest(paths.publish.root))
    .pipe($.size({title: 'Package JSON: '}))
});

gulp.task('tag', function (db) {
  var message = [ 'Release: ', 'v', pkg.version].join('');

  return gulp.src(paths.publish.root)
    .pipe($.git.add({ args: '--all ./' }))
    .pipe($.git.commit(message, { args: '-a' }))
    .pipe(gulp.src(paths.publish.packages[0]))
    .pipe($.tagVersion())
    .pipe($.git.push('origin', 'master', { args: '--tags' }));
});

gulp.task('npm', function (cb) {
  return require('child_process')
    .spawn('npm', ['publish'], { stdio: 'inherit' })
    .on('close', cb);
});

// ==================================== BUILD ================================================== //
// Main Tasks
gulp.task('dev',     ['test', 'watch']);

gulp.task('clean',   ['clean-dev', 'clean-prod']);

gulp.task('default', ['vendor', 'templates', 'styles', 'scripts', 'app']);



// Production Tasks
gulp.task('publish', ['default'], function(cb){
  return $.runSequence('bump', 'tag', /*'npm',*/ cb);
})

gulp.task('prod',    ['default'], function(cb){
  return $.runSequence([ 'clean-dev', 'manifest' ], cb);
});

gulp.task('test',    ['default'], function(cb){
  return $.runSequence([ /*lint, 'karma',*/ 'mocha', 'jasmine' ], cb);
});

